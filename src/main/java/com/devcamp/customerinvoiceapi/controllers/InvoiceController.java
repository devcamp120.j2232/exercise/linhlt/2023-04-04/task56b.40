package com.devcamp.customerinvoiceapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerinvoiceapi.Customer;
import com.devcamp.customerinvoiceapi.Invoice;

@RestController
@RequestMapping("/")
@CrossOrigin
public class InvoiceController {
    @GetMapping("/invoices")
    public ArrayList<Invoice> getInvoicesApi(){
        //task 4
        Customer customer1 = new Customer(1, "Linh", 40);
        Customer customer2 = new Customer(2, "Minh", 10);
        Customer customer3 = new Customer(2, "Trang", 20);
        System.out.println(customer1);
        System.out.println(customer2);
        System.out.println(customer3);
        //task 5
        Invoice invoice1 = new Invoice(1001, customer1, 1000000 );
        Invoice invoice2 = new Invoice(1002, customer2, 500000 );
        Invoice invoice3 = new Invoice(1003, customer3, 450000 );
        System.out.println(invoice1);
        System.out.println(invoice2);
        System.out.println(invoice3);
        //task 6
        ArrayList<Invoice> invoiceList = new ArrayList<>();
        invoiceList.add(invoice1);
        invoiceList.add(invoice2);
        invoiceList.add(invoice3);
        return invoiceList;
    }
}
